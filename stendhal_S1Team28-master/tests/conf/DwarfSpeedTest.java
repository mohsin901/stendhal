package conf;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import marauroa.common.Log4J;

public class DwarfSpeedTest {
	private static final float DWARF_SPEED_UNDER_LEVEL_30 = 0.8f;

	@Test
	public void testDwarfSpeed() {
		Log4J.init();
		try {
			//Initialise doc
			final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

			//Load file dwarf xml
			final File file = new File("data/conf/creatures/dwarf.xml");

			assertThat(file, notNullValue());
			
			//Parse xml
			final Document doc = docBuilder.parse(file);
			proceedDocument(doc);

		} catch (final SAXParseException err) {

			fail(err.toString());

		} catch (final SAXException e) {

			fail(e.toString());
		} catch (final Exception t) {

			fail(t.toString());
		}
	}
	
	void proceedDocument(final Document xmldoc) {
		//Normalise text representation
		xmldoc.getDocumentElement().normalize();

		//Get dwarfs
		final NodeList listOfDwarfs = xmldoc.getElementsByTagName("creature");
		
		if (listOfDwarfs.getLength() > 0) {
			for (int s = 0; s < listOfDwarfs.getLength(); s++) {
				float speed = 0;
				int level = 0;
				//Get dwarf names
				String dwarfName = listOfDwarfs.item(s).getAttributes().getNamedItem("name").getNodeValue();
				
				final NodeList listOfChildren = listOfDwarfs.item(s).getChildNodes();
				for (int i = 0; i < listOfChildren.getLength(); i++) {
					if (listOfChildren.item(i).getNodeName().equals("attributes")) {
						final NodeList listOfAttributes = listOfChildren.item(i).getChildNodes();
						for (int j = 0; j < listOfAttributes.getLength(); j++) {
							//get speed
							if (listOfAttributes.item(j).getNodeName().equals("speed")) {
								speed = Float.parseFloat(listOfAttributes.item(j).getAttributes().getNamedItem(
										"value").getNodeValue());
								break;
							}
						}
					}
					
					//Get dwarf levels
					if (listOfChildren.item(i).getNodeName().equals("level")) {
						level = Integer.parseInt(listOfChildren.item(i).getAttributes()
								.getNamedItem("value").getNodeValue());
					}
				}
				
				//Assert - Every dwarf creature under level 30 must have the same speed
				if(level <= 30)
					assertTrue(String.format("Level of %s is %d and speed is %.2f instead of %.2f", dwarfName, level, speed, DWARF_SPEED_UNDER_LEVEL_30),
							Float.compare(speed, DWARF_SPEED_UNDER_LEVEL_30) == 0);
			}
		}
	}
}

