package games.stendhal.client.actions;

import static org.junit.Assert.*;

import org.junit.Test;

public class SlashAttributesTest {
	
	//Tests for createslashattributes
	@Test
	public void testcreateSlashAttributes() {
		SlashAttributes remainderAttribute = SlashAttributes.createSlashAttributes("fuel");
		SlashAttributes literalAttribute = SlashAttributes.createSlashAttributes("car","value");
		SlashAttributes paramAttribute = SlashAttributes.createSlashAttributes("drive",0);
		
		assertTrue(remainderAttribute != null);
		assertTrue(literalAttribute != null);
		assertTrue(paramAttribute != null);
	}
	
	//Tests for getInput_paramater
	@Test
	public void testGetInputParamater() {
		SlashAttributes remainderAttribute = SlashAttributes.createSlashAttributes("fuel");
		SlashAttributes literalAttribute = SlashAttributes.createSlashAttributes("car","value");
		SlashAttributes paramAttribute = SlashAttributes.createSlashAttributes("drive",0);
		
		assertTrue(remainderAttribute.getInput_paramater() == ParamNames.REMAINDER);
		assertTrue(literalAttribute.getInput_paramater() == ParamNames.LITERAL);
		assertTrue(paramAttribute.getInput_paramater() == ParamNames.PARAM);
	}
	
	//Tests for getValue
	@Test
	public void testGetValue() {
		SlashAttributes remainderAttribute = SlashAttributes.createSlashAttributes("fuel");
		SlashAttributes literalAttribute = SlashAttributes.createSlashAttributes("car","value");
		SlashAttributes paramAttribute = SlashAttributes.createSlashAttributes("drive",0);
		
		assertEquals(remainderAttribute.getValue(),"fuel");
		assertEquals(literalAttribute.getValue(),"car");
		assertEquals(paramAttribute.getValue(),"drive");
	}
	
	//Tests for getTypeLiteral
	@Test
	public void testGetTypeLiteral() {
		SlashAttributes remainderAttribute = SlashAttributes.createSlashAttributes("fuel");
		SlashAttributes literalAttribute = SlashAttributes.createSlashAttributes("car","value");
		SlashAttributes paramAttribute = SlashAttributes.createSlashAttributes("drive",0);
		
		assertTrue(remainderAttribute.getTypeLiteral() == null);
		assertEquals(literalAttribute.getTypeLiteral(),"value");
		assertTrue(paramAttribute.getTypeLiteral() == null);
	}
	
	//Tests for getTypeParam
	@Test
	public void testGetTypeParam() {
		SlashAttributes remainderAttribute = SlashAttributes.createSlashAttributes("fuel");
		SlashAttributes literalAttribute = SlashAttributes.createSlashAttributes("car","value");
		SlashAttributes paramAttribute = SlashAttributes.createSlashAttributes("drive",0);
		
		assertTrue(remainderAttribute.getTypeParam() == null);
		assertTrue(literalAttribute.getTypeParam() == null);
		assertTrue(paramAttribute.getTypeParam() == 0);

	}
}
