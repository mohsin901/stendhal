package games.stendhal.client.actions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import games.stendhal.client.MockStendhalClient;
import games.stendhal.client.StendhalClient;
import marauroa.common.game.RPAction;
import static games.stendhal.common.constants.Actions.TELEPORT;

public class TeleportActionTest {
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		StendhalClient.resetClient();
	}
	
	//tests the teleport action
	@Test
	public void testTeleportAction() {
		new MockStendhalClient() {
			@Override
			public void send(final RPAction action) {
				System.out.println("action : " + action.get("type"));
				assertEquals("teleport", action.get("type"));
				assertEquals("patrick", action.get("target"));
				assertEquals("semos", action.get("zone"));
				assertEquals("127", action.get("x"));
				assertEquals("128", action.get("y"));
			}
		};
	
	
		final DefaultAction teleportAction = new DefaultAction(4, 4, TELEPORT);
		teleportAction.setCommand("teleport");
		teleportAction.set_is_reminder(false);
		SlashAttributes attr1 = new SlashAttributes(ParamNames.PARAM, "target", 0);
		SlashAttributes attr2 = new SlashAttributes(ParamNames.PARAM, "zone", 1);
		SlashAttributes attr3 = new SlashAttributes(ParamNames.PARAM, "x", 2);
		SlashAttributes attr4 = new SlashAttributes(ParamNames.PARAM, "y", 3);
		teleportAction.put_attr(attr1);
		teleportAction.put_attr(attr2);
		teleportAction.put_attr(attr3);
		teleportAction.put_attr(attr4);
		boolean result = teleportAction.execute(new String[]{"patrick", "semos", "127", "128"}, null);
		assertTrue(result);
		
		final TeleportAction action = new TeleportAction();
		assertTrue(action.execute(new String[]{"patrick", "semos", "127", "128"}, null));
	}
	@Test
	public void testGetMaximumParameters() {
		final TeleportAction action = new TeleportAction();
		assertThat(action.getMaximumParameters(), is(4));
	}
	/**
	 * Tests for getMinimumParameters.
	 */
	@Test
	public void testGetMinimumParameters() {
		final TeleportAction action = new TeleportAction();
		assertThat(action.getMinimumParameters(), is(4));
	}
}
