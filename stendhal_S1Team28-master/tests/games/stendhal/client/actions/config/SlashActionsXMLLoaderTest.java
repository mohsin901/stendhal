package games.stendhal.client.actions.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


import games.stendhal.client.actions.DefaultAction;
//import games.stendhal.client.actions.SlashAttributes;
//import games.stendhal.client.actions.ParamNames;

public class SlashActionsXMLLoaderTest {

	@Test
	public void checkLoad() throws URISyntaxException, SAXException {
		SlashActionsXMLLoader actionLoader= new SlashActionsXMLLoader();

		List <DefaultAction> actions = actionLoader.load(new URI("SlashActionsTest.xml"));

		DefaultAction action = actions.get(0);
		assertThat(action.getCommand(), is("grumpy"));
		assertThat(action.get_type_of_our_action(), is("grumpy"));
		assertThat(action.getMinimumParameters(), is(Integer.valueOf(0)));
		assertThat(action.getMaximumParameters(), is(Integer.valueOf(0)));

		//List < SlashAttributes > attributes = action.get_our_attr();


		//SlashAttributes attribute = attributes.get(0);
		//assertThat(attribute.getValue(), is("reason"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.PARAM));
		//assertThat(attribute.getTypeLiteral(), is(0));

		//attribute = attributes.get(1);
		//assertThat(attribute.getValue(), is("zone"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.PARAM));
		//assertThat(attribute.getTypeLiteral(), is(1));

		//attribute = attributes.get(2);
		//assertThat(attribute.getValue(), is("x"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.PARAM));
		//assertThat(attribute.getTypeLiteral(), is(2));

		//attribute = attributes.get(3);
		//assertThat(attribute.getValue(), is("y"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.PARAM));
		//assertThat(attribute.getTypeLiteral(), is(2));


		//action = actions.get(1);
		//assertThat(action.getCommand(), is("teleport"));
		//assertThat(action.get_type_of_our_action(), is("challenge"));
		//assertThat(action.getMinimumParameters(), is(Integer.valueOf(4)));
		//assertThat(action.getMaximumParameters(), is(Integer.valueOf(4)));

		//attributes = action.get_our_attr();

		//attribute = attributes.get(0);
		//assertThat(attribute.getValue(), is("action"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.LITERAL));
		//assertThat(attribute.getTypeLiteral(), is("accept"));

		//attribute = attributes.get(1);
		//assertThat(attribute.getValue(), is("target"));
		//assertThat(attribute.getTypeParam(), is(ParamNames.PARAM));
		//assertThat(attribute.getTypeLiteral(), is(0));
	}
}

