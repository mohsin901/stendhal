package games.stendhal.client.actions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.After;
import org.junit.BeforeClass;

import games.stendhal.client.MockStendhalClient;
import games.stendhal.client.StendhalClient;
import marauroa.common.game.RPAction;

public class DefaultActionTest {

	static DefaultAction teleportAction;
	static DefaultAction grumpyAction;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		teleportAction = new DefaultAction(4, 4, "teleport");
		teleportAction.set_is_reminder(false);
		grumpyAction = new DefaultAction(0, 0, "grumpy");
		grumpyAction.set_is_reminder(true);
	}

	@After
	public void tearDown() throws Exception {
		StendhalClient.resetClient();
	}

	@Test
	public void testExecute() {
		new MockStendhalClient() {
			@Override
			public void send(final RPAction action) {
				switch (action.get("type")) {
				case "grumpy":
					assertEquals("grumpy", action.get("type"));
					assertEquals("remainder", action.get("reason"));

					break;
				case "teleport":
					System.out.println("action : " + action.get("type"));

					assertEquals("teleport", action.get("type"));
					assertEquals("target_attribute", action.get("target"));
					assertEquals("zone_attribute", action.get("zone"));
					assertEquals("10", action.get("x"));
					assertEquals("20", action.get("y"));
					break;
				}
			}
		};

		teleportAction.reset_our_attr();
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("target", 0));
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("zone", 1));
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("x", 2));
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("y", 3));
		assertTrue(teleportAction.execute(new String[] {"target_attribute", "zone_attribute", "10", "20" }, null));
		
		grumpyAction.reset_our_attr();
		grumpyAction.put_attr(SlashAttributes.createSlashAttributes("reason"));
		assertTrue(grumpyAction.execute(new String[] {}, "remainder"));

	}

	/**
	 * Tests for getMaximumParameters.
	 */
	@Test
	public void testGetMaximumParameters() {
		assertThat(teleportAction.getMaximumParameters(), is(4));
		assertThat(grumpyAction.getMaximumParameters(), is(0));
	}
	/**
	 * Tests for getMinimumParameters.
	 */
	@Test
	public void testGetMinimumParameters() {
		assertThat(teleportAction.getMinimumParameters(), is(4));
		assertThat(grumpyAction.getMinimumParameters(), is(0));
	}
	
	 @Test
	   public void testPut_attr() {
	      teleportAction.reset_our_attr();
	      teleportAction.put_attr(SlashAttributes.createSlashAttributes("target", 0)); 
	      assertEquals("target", teleportAction.get_our_attr().get(0).getValue());
	   }
		
	@Test
	public void TestSet_our_attr() {
		List < SlashAttributes > attr = new ArrayList < SlashAttributes > ();
		attr.add(new SlashAttributes(ParamNames.REMAINDER, "test", 0)); 
		teleportAction.set_our_attr(attr);
		assertEquals(teleportAction.get_our_attr(), attr);
		
		grumpyAction.set_our_attr(attr);
		assertEquals(grumpyAction.get_our_attr(), attr);
	}
	
	@Test
	public void TestReset_our_attr() {
		
		teleportAction.reset_our_attr();
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("target", 0));
		teleportAction.reset_our_attr();
		assertEquals(0, teleportAction.get_our_attr().size());
	}
	
	@Test
	public void TestGet_our_attr() {
		teleportAction.reset_our_attr();
		teleportAction.put_attr(SlashAttributes.createSlashAttributes("target", 0)); 
		assertEquals("target", teleportAction.get_our_attr().get(0).getValue());
	}
	
	@Test
	public void Testget_type_of_our_action() {
		assertEquals("teleport", teleportAction.get_type_of_our_action());
		assertEquals("grumpy", grumpyAction.get_type_of_our_action());
	}
	
	@Test
	public void TestGet_is_reminder() {
		assertFalse(teleportAction.get_is_reminder());
		assertTrue(grumpyAction.get_is_reminder());
	}
	
	@Test
	public void TestSet_is_reminder() {
		boolean b = teleportAction.get_is_reminder();
		teleportAction.set_is_reminder(!(teleportAction.get_is_reminder()));
		assertTrue(b != teleportAction.get_is_reminder());
		teleportAction.set_is_reminder(false);
		
		b = grumpyAction.get_is_reminder();
		grumpyAction.set_is_reminder(!(grumpyAction.get_is_reminder()));
		assertTrue(b != grumpyAction.get_is_reminder());
		teleportAction.set_is_reminder(true);
	}
	
	@Test
	public void TestGetCommand() {
		teleportAction.setCommand(" ");  
		assertEquals(" ", teleportAction.getCommand());
		
		grumpyAction.setCommand(" "); 
		assertEquals(" ", grumpyAction.getCommand());
	}

	@Test
	public void TestSetCommand() {
		 
		teleportAction.setCommand(" "); 
		assertEquals(" ", teleportAction.getCommand());
		
		grumpyAction.setCommand(" "); 
		assertEquals(" ", grumpyAction.getCommand());
	}
	
}

