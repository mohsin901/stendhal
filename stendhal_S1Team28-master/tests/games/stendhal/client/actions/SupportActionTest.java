package games.stendhal.client.actions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import games.stendhal.client.MockStendhalClient;
import marauroa.common.game.RPAction;

public class SupportActionTest {
	/**
	 * Tests for getMaximumParameters.
	 */
	@Test
	public void testGetMaximumParameters() {
		final SupportAction action = new SupportAction();
		assertThat(action.getMaximumParameters(), is(0));
	}
	/**
	 * Tests for getMinimumParameters.
	 */
	@Test
	public void testGetMinimumParameters() {
		final SupportAction action = new SupportAction();
		assertThat(action.getMinimumParameters(), is(0));
	}
	
	@Test
	public void testExecute() {
		new MockStendhalClient() {
			@Override
			public void send(final RPAction action) {
				assertEquals("support", action.get("type"));
				assertEquals("remainder", action.get("text"));
				
			}
		};
		final SupportAction action = new SupportAction();
		assertTrue(action.execute(new String[] {}, "remainder"));
	}
}
