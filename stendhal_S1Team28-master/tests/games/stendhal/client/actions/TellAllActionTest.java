package games.stendhal.client.actions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import games.stendhal.client.MockStendhalClient;
import marauroa.common.game.RPAction;

public class TellAllActionTest {
	/**
	 * Tests for getMaximumParameters.
	 */
	@Test
	public void testGetMaximumParameters() {
		final TellAllAction action = new TellAllAction();
		assertThat(action.getMaximumParameters(), is(0));
	}
	/**
	 * Tests for getMinimumParameters.
	 */
	@Test
	public void testGetMinimumParameters() {
		final TellAllAction action = new TellAllAction();
		assertThat(action.getMinimumParameters(), is(0));
	}
	
	@Test
	public void testExecute() {
		new MockStendhalClient() {
			@Override
			public void send(final RPAction action) {
				assertEquals("tellall", action.get("type"));
				assertEquals("remainder", action.get("text"));
				
			}
		};
		final TellAllAction action = new TellAllAction();
		assertTrue(action.execute(new String[] {}, "remainder"));
	}
}
