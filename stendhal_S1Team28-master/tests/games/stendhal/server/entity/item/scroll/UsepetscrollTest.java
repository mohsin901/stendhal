package games.stendhal.server.entity.item.scroll;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import games.stendhal.server.core.engine.SingletonRepository;
import games.stendhal.server.core.engine.StendhalRPZone;
import games.stendhal.server.entity.creature.Cat;
import games.stendhal.server.entity.creature.Pet;
import games.stendhal.server.entity.player.Player;
import games.stendhal.server.maps.MockStendlRPWorld;
import utilities.PlayerTestHelper;
import utilities.RPClass.GrowingPassiveEntityRespawnPointTestHelper;
import utilities.RPClass.ItemTestHelper;


public class UsepetscrollTest {
	private Pet pet;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ItemTestHelper.generateRPClasses();
		MockStendlRPWorld.get();
		GrowingPassiveEntityRespawnPointTestHelper.generateRPClasses();
	}
	
	@Test
	public void testuseScroll() {
		final BlankPetScroll bps = (BlankPetScroll) SingletonRepository.getEntityManager().getItem("blank pet scroll");
		final Player player = PlayerTestHelper.createPlayer("bob");
		final StendhalRPZone zone = new StendhalRPZone("zone");
		SingletonRepository.getRPWorld().addRPZone(zone);
		zone.add(player);
		pet = new Cat(player);
		assertTrue(bps.useScroll(player));
		
		pet = player.getPet();
		pet = null;
		assertFalse(bps.useScroll(player));
	}
}
