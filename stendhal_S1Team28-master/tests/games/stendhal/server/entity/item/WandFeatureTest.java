package games.stendhal.server.entity.item;


import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import games.stendhal.server.core.engine.SingletonRepository;
import games.stendhal.server.core.engine.StendhalRPZone;
import games.stendhal.server.entity.creature.Creature;
import games.stendhal.server.entity.player.Player;
import games.stendhal.server.events.AttackEvent;
import games.stendhal.server.maps.MockStendhalRPRuleProcessor;
import games.stendhal.server.maps.MockStendlRPWorld;
import marauroa.common.game.RPEvent;
import utilities.PlayerTestHelper;
import utilities.RPClass.ItemTestHelper;

public class WandFeatureTest {
	private Item wand;
	private Item magic_arrow;
	private Player player;
	private Creature creature;
	private StendhalRPZone zone;
	private boolean attack;
	private AttackEvent atk;
	private int damageValue;
	private boolean hit;
	private String v;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		MockStendlRPWorld.get();
		MockStendhalRPRuleProcessor.get();
		ItemTestHelper.generateRPClasses();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		MockStendlRPWorld.reset();
	}
	
	@Before
	public void setUp() {
		this.wand = SingletonRepository.getEntityManager().getItem("wand_of_sluggishness");
		this.magic_arrow = SingletonRepository.getEntityManager().getItem("magic arrow");
	}
	
	@Test
	public void testWandCanAttack() throws Exception {
		assertNotNull(wand);
		zone = new StendhalRPZone("testzone");
		creature = SingletonRepository.getEntityManager().getCreature("Wolf");
		player = PlayerTestHelper.createPlayer("bob");
		player.getSlot("lhand").add(wand);
		player.getSlot("rhand").add(magic_arrow);
		zone.add(player);
		zone.add(creature);
	
		player.setTarget(creature);
		assertNotNull(player.getAttackTarget());
		
		hit = player.canHit(creature);
		hit = true;
		damageValue = player.damageDone(creature, magic_arrow.getAttack(), null);
		v = Integer.toString(damageValue);

		assertTrue(zone.has(creature.getID()));
		assertThat(creature.getHP(), greaterThan(0));
		for (RPEvent ev : player.events()) {
			assertFalse(ev instanceof AttackEvent);
		}
		
		player.attack();
		
		atk = null;
		for (RPEvent ev : player.events()) {
			if (ev instanceof AttackEvent) {
				atk= (AttackEvent) ev;
				continue;
			}
		}
		
		assertTrue(atk.has("hit"));
		assertTrue(atk.has("damage"));
		assertThat("no damage done ", atk.get("damage"), is(v));	
	}
}
