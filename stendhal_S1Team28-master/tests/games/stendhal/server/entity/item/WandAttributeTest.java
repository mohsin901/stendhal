package games.stendhal.server.entity.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import games.stendhal.server.core.engine.SingletonRepository;
import games.stendhal.server.entity.player.Player;
import games.stendhal.server.maps.MockStendhalRPRuleProcessor;
import games.stendhal.server.maps.MockStendlRPWorld;
import utilities.PlayerTestHelper;
import utilities.RPClass.ItemTestHelper;

public class WandAttributeTest {

	@BeforeClass
	public static void setUpBeforeClass() {
		MockStendlRPWorld.get();
		MockStendhalRPRuleProcessor.get();
		ItemTestHelper.generateRPClasses();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		MockStendlRPWorld.reset();
	}
	
	@Test
	public void testAttributeofWand() {
		final Item wand = SingletonRepository.getEntityManager().getItem("wand_of_sluggishness");
		final Item m_arrow = SingletonRepository.getEntityManager().getItem("magic arrow");
		Player player = PlayerTestHelper.createPlayer("bob");
		player.equip("lhand", wand);
		player.equip("rhand", m_arrow);
		
		//test attribute of wand
		assertNotNull(wand);
		assertEquals(0, wand.getAttack());	
		assertEquals(3, wand.getAttackRate());	
		assertTrue(wand.canBeEquippedIn("lhand"));
		
		//test magic arrow
		assertNotNull(m_arrow);
		assertEquals(0, m_arrow.getAttack());	
		assertTrue(m_arrow.canBeEquippedIn("rhand"));
	}



}
