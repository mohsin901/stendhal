package games.stendhal.server.entity.creature;

import static org.junit.Assert.*;
import org.junit.Test;

import games.stendhal.server.entity.item.Item;
import games.stendhal.server.entity.player.Player;
import games.stendhal.server.maps.MockStendlRPWorld;
import games.stendhal.server.core.engine.SingletonRepository;
import games.stendhal.server.core.engine.StendhalRPZone;
import utilities.PlayerTestHelper;

public class PetPotionTest {
	@Test
	public void petPotionTest() {
		/**Tests for preventing pets taking potions with high HP*/
		final StendhalRPZone zone = new StendhalRPZone("zone");
		MockStendlRPWorld.get().addRPZone(zone);
		
		final Player player = PlayerTestHelper.createPlayer("bob");
		final Pet pet = new Cat();
		zone.add(player);
		zone.add(pet);
		player.setPet(pet);
		
		assertTrue(player.hasPet());
		
		Item potion = SingletonRepository.getEntityManager().getItem("potion");
		zone.add(potion);
		
		pet.setHP(100);
		assertTrue(pet.getHP() == 100);	
		pet.logic();
		assertTrue(pet.getHP() == 100);
		
		pet.setHP(99);
		assertTrue(pet.getHP() == 99);
		pet.logic();
		assertTrue(pet.getHP() != 99);
	}
}
