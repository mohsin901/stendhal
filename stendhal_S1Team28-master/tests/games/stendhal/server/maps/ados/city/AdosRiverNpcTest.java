package games.stendhal.server.maps.ados.city;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static utilities.SpeakerNPCTestHelper.getReply;
import utilities.ZonePlayerAndNPCTestImpl;
import games.stendhal.server.entity.npc.SpeakerNPC;
import games.stendhal.server.entity.npc.fsm.Engine;

public class AdosRiverNpcTest extends ZonePlayerAndNPCTestImpl{
	private static final String ZONE_NAME = "testzone";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		setupZone(ZONE_NAME);
	}
	
	public AdosRiverNpcTest() {
		setNpcNames("Tourguide Of Ados");
		setZoneForPlayer(ZONE_NAME);
		addZoneConfigurator(new AdosRiverNpc(), ZONE_NAME); //create new class
	}
	

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();

	}
	
	@Test
	public void testHi() {
		
		final SpeakerNPC npc = getNPC("Tourguide Of Ados");     
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // User says hi and NPC should reply with this statement
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));

	}
	
	@Test
	public void testHiAndHelp() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      // Create an engine instance
		final Engine en = npc.getEngine();             
		
		en.step(player, "hi");                         // User says hi and NPCS should reply with this statement
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "help");     
		assertEquals("Use the command 'transport', 'ticket', or 'job' to recieve updates on new transport and on the price of the tickets aswell as finding out my job.", getReply(npc));
	}
	

	@Test
	public void testHiThenJob() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // User says hi and NPC should reply with this statement
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "job");     
		assertEquals("I am the new tourguide in Ados, I will accompany users on the ferry to the city of Deniran and give them interesting facts.", getReply(npc)); //new tour guide
	}
	
	@Test
	public void testHiAndTicket() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // User says hi and NPC should reply with this statement
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "ticket");     
		assertEquals("A one way ticket to Deniran will be 5 cash, and a return costs 8. The prices may change in the future however.", getReply(npc));
	}

	
	@Test
	public void testHiAndTransport() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // Say hi to invoke first
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "transport");     
		assertEquals("A new ferry will be deployed at the shore of the rivers in Deniran and Ados. They're will be free snacks onboard, make sure you check this out!", getReply(npc));
	}


	@Test
	public void testHiAndHelpAndTransportAndTicket() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // Say hi to invoke first
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "help");     
		assertEquals("Use the command 'transport', 'ticket', or 'job' to recieve updates on new transport and on the price of the tickets aswell as finding out my job.", getReply(npc));
		
		en.step(player, "transport");
		assertEquals("A new ferry will be deployed at the shore of the rivers in Deniran and Ados. They're will be free snacks onboard, make sure you check this out!", getReply(npc));

		en.step(player, "ticket");     
		assertEquals("A one way ticket to Deniran will be 5 cash, and a return costs 8. The prices may change in the future however.", getReply(npc));
	}
	
	
	@Test
	public void testHiAndJobAndTransport() {

		final SpeakerNPC npc = getNPC("Tourguide Of Ados");      
		final Engine en = npc.getEngine();             // Create an engine instance
		
		en.step(player, "hi");                         // Say hi to invoke first
		assertEquals("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?", getReply(npc));
		
		en.step(player, "job");     
		assertEquals("I am the new tourguide in Ados, I will accompany users on the ferry to the city of Deniran and give them interesting facts.", getReply(npc)); //new tour guide
		
		en.step(player, "transport");
		assertEquals("A new ferry will be deployed at the shore of the rivers in Deniran and Ados. They're will be free snacks onboard, make sure you check this out!", getReply(npc));

	}
	

	
}

