package games.stendhal.server.maps.deniran.cityinterior.hospital;

import static org.junit.Assert.*;
import static utilities.SpeakerNPCTestHelper.getReply;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import games.stendhal.server.entity.npc.SpeakerNPC;
import games.stendhal.server.entity.npc.fsm.Engine;
import utilities.ZonePlayerAndNPCTestImpl;

public class NurseNPCTest extends ZonePlayerAndNPCTestImpl {

	private static final String ZONE_NAME = "testzone";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
			setupZone(ZONE_NAME);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}
	
	public NurseNPCTest() {
		setNpcNames("Nurse 1");
		setZoneForPlayer(ZONE_NAME);
		addZoneConfigurator(new Nurse1NPC(), ZONE_NAME);
	}

	@Test
	public void testHiAndBye() {
		final SpeakerNPC npc = getNPC("Nurse 1");
		final Engine en = npc.getEngine();
		
		assertTrue(en.step(player, "hi"));
		assertEquals("Hello, welcome to Deniran hospital!", getReply(npc));
		
		assertTrue(en.step(player, "bye"));
		assertEquals("Take care!", getReply(npc));
	}
	
	@Test
	public void testJobAndOffer() {

		final SpeakerNPC npc = getNPC("Nurse 1");      
		final Engine en = npc.getEngine();             
		     
		assertTrue(en.step(player, "hi"));                        
		assertEquals("Hello, welcome to Deniran hospital!", getReply(npc));
		      
		assertTrue(en.step(player, "job"));   
		assertEquals("I'm a Nurse of Deniran Hospital.", getReply(npc));
		
		assertTrue(en.step(player, "help"));     
		assertEquals("This is going to be the best hospital in the world!", getReply(npc));
		      
	}
	
	@Test
	public void testHiAndJobAndOfferAndGoodbye() {

		final SpeakerNPC npc = getNPC("Nurse 1");      
		final Engine en = npc.getEngine();             
		     
		assertTrue(en.step(player, "hi"));                        
		assertEquals("Hello, welcome to Deniran hospital!", getReply(npc));
		      
		assertTrue(en.step(player, "job"));   
		assertEquals("I'm a Nurse of Deniran Hospital.", getReply(npc));
		
		assertTrue(en.step(player, "help"));     
		assertEquals("This is going to be the best hospital in the world!", getReply(npc));
		      
		assertTrue(en.step(player, "bye")); 
	    assertEquals("Take care!", getReply(npc));
		      
	}
	
			
}
