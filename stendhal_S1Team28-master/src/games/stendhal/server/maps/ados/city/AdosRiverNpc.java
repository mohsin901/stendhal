package games.stendhal.server.maps.ados.city;

import java.util.Map;

import games.stendhal.server.core.config.ZoneConfigurator;
import games.stendhal.server.core.engine.StendhalRPZone;
import games.stendhal.server.entity.npc.SpeakerNPC;
import games.stendhal.common.Direction;

public class AdosRiverNpc implements ZoneConfigurator {

	@Override
	public void configureZone(StendhalRPZone zone, Map<String, String> attributes) {
		// TODO Auto-generated method stub
		buildNPC(zone);
	}

	private void buildNPC(final StendhalRPZone zone) {
    	final SpeakerNPC npc = new SpeakerNPC("Tourguide Of Ados") {
    	    @Override
            protected void createPath() {
                setPath(null);
            }
            @Override
			protected void createDialog() {
				addGreeting("Hello player, I will be the new tourguide once we purchase the ferry. How may I be of assistance to you today?");
				addHelp("Use the command 'transport', 'ticket', or 'job' to recieve updates on new transport and on the price of the tickets aswell as finding out my job.");
				addJob("I am the new tourguide in Ados, I will accompany users on the ferry to the city of Deniran and give them interesting facts.");
				addReply("ticket","A one way ticket to Deniran will be 5 cash, and a return costs 8. The prices may change in the future however.");
				addReply("transport","A new ferry will be deployed at the shore of the rivers in Deniran and Ados. They're will be free snacks onboard, make sure you check this out!");
				addGoodbye();
			}
		};

		npc.setEntityClass("sailor1npc");
		npc.setDescription("You see Tourguide Of Ados, he is always willing to help.");
		npc.setPosition(79,55);
		npc.setDirection(Direction.LEFT);
		npc.initHP(100);
		zone.add(npc);
	}
}
