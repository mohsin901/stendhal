package games.stendhal.server.maps.deniran.cityinterior.hospital;

import games.stendhal.server.core.config.ZoneConfigurator;
import games.stendhal.server.core.engine.StendhalRPZone;
import games.stendhal.server.entity.npc.SpeakerNPC;

import java.util.Map;

public class Nurse1NPC implements ZoneConfigurator {

	@Override
	public void configureZone(StendhalRPZone zone, Map<String, String> attributes) {
		buildNPC(zone);
	}
	
    	private void buildNPC(final StendhalRPZone zone) {
	    final SpeakerNPC npc = new SpeakerNPC("Nurse 1") {
        @Override
		protected void createPath() {
        	setPath(null);
        }
        
        @Override
        protected void createDialog() {
            addGreeting("Hello, welcome to Deniran hospital!");
            addJob("I'm a Nurse of Deniran Hospital.");
            addHelp("This is going to be the best hospital in the world!");
            addGoodbye("Take care!");
        }
    };
    
    npc.setEntityClass("doctornpc");
    npc.setDescription("You see a nurse, they look a bit busy at the moment but perhaps they can help you anyway.");
    npc.initHP(100);
    npc.setPosition(8, 8);
    zone.add(npc);   
    }
}