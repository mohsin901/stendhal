package games.stendhal.server.maps.deniran.cityinterior.hospital;

import games.stendhal.server.core.config.ZoneConfigurator;
import games.stendhal.server.core.engine.StendhalRPZone;
import games.stendhal.server.entity.npc.SpeakerNPC;

import java.util.Map;

public class Doctor2NPC implements ZoneConfigurator {

	@Override
	public void configureZone(StendhalRPZone zone, Map<String, String> attributes) {
		buildNPC(zone);
	}
	
    	private void buildNPC(final StendhalRPZone zone) {
	    final SpeakerNPC npc = new SpeakerNPC("Doctor 2") {
        @Override
		protected void createPath() {
        	setPath(null);
        }
        
        @Override
        protected void createDialog() {
            addGreeting("Hello, welcome to Deniran hospital!");
            addJob("I'm a Doctor of Deniran Hospital.");
            addHelp("Soon, I'll be able to heal you in a ward.");
            addGoodbye("Take care!");
        }
    };
    
    npc.setEntityClass("doctornpc");
    npc.setDescription("You see a doctor, they look a bit busy at the moment but perhaps they can help you anyway.");
    npc.initHP(100);
    npc.setPosition(6, 3);
    zone.add(npc);   
    }
}