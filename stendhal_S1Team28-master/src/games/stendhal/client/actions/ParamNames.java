package games.stendhal.client.actions;

public enum ParamNames {
	LITERAL,
	REMAINDER,
	PARAM
}