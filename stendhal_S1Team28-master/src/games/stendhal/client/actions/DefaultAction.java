package games.stendhal.client.actions;
import games.stendhal.client.ClientSingletonRepository;
import marauroa.common.game.RPAction;
import java.util.List;
import java.util.ArrayList;

/**
 * Default Action class intends to replace multiple action classes which have a similar execute structure
 * It takes the remainder and paramater imputes and handles them as required.
 */

public class DefaultAction implements SlashAction {
	private String command;
	private Boolean is_remainder_req = false;
	private int smallest_param;
	private int biggest_param;
	private List < SlashAttributes > properties;
	private String type_of_action;


	public DefaultAction(int smallest_param, int biggest_param, String type_of_action) {
		this.smallest_param = smallest_param;
		this.type_of_action = type_of_action;
		this.properties = new ArrayList < SlashAttributes > ();
		this.biggest_param = biggest_param;
	}

	public void put_attr(SlashAttributes attr) {
		properties.add(attr);
	}

	public void set_our_attr(List < SlashAttributes > attr) {
		this.properties = attr;
	}

	public void reset_our_attr() {
		properties.clear();
	}
	public List < SlashAttributes > get_our_attr() {
		return properties;
	}

	@Override
	public int getMaximumParameters() {
		return biggest_param;
	}

	@Override
	public int getMinimumParameters() {
		return smallest_param;
	}

	public String get_type_of_our_action() {
		return type_of_action;
	}

	public Boolean get_is_reminder() {
		return is_remainder_req;
	}

	public void set_is_reminder(Boolean req) {
		is_remainder_req = req;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public boolean execute(final String[] params, final String remainder) {
		if (params == null || params.length < smallest_param || params.length > biggest_param ||
				(is_remainder_req && (remainder == null || remainder.isEmpty()))) {
			return false;
		}
		final RPAction our_slash_action = new RPAction();
		our_slash_action.put("type", type_of_action);
		for (SlashAttributes attribute: properties) {
			if (attribute.getInput_paramater() == ParamNames.REMAINDER) {
				our_slash_action.put(attribute.getValue(), remainder);
			} else if (attribute.getInput_paramater() == ParamNames.LITERAL) {
				our_slash_action.put(attribute.getValue(), attribute.getTypeLiteral());
			} else {
				int position = attribute.getTypeParam();
				if (position < params.length) {
					our_slash_action.put(attribute.getValue(), params[attribute.getTypeParam()]);
				}
			}
		}
		ClientSingletonRepository.getClientFramework().send(our_slash_action);

		return true;
	}



}