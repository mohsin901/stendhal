package games.stendhal.client.actions.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;

import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import games.stendhal.client.actions.SlashAttributes;
import games.stendhal.client.actions.ParamNames;
import games.stendhal.client.actions.DefaultAction;


public final class SlashActionsXMLLoader extends DefaultHandler {
	private static final Logger LOGGER = Logger.getLogger(SlashActionsXMLLoader.class);
	private String value_text;
	private List < SlashAttributes > attributes;
	private ParamNames input_paramater;
	private String my_value;
	private String type_literal;
	private int type_param;
	private Boolean tag_of_attr = false;
	private int min_param;
	private int max_param;
	private String type_of_action;
	private String command;
	private Boolean use_of_remainder = false;
	private List < DefaultAction > my_action_list;


	public List < DefaultAction > load(final URI uri) throws SAXException {
		my_action_list = new LinkedList < DefaultAction > ();
		final SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			final SAXParser saxParser = factory.newSAXParser();

			final InputStream is = SlashActionsXMLLoader.class.getResourceAsStream(uri.getPath());

			if (is == null) {
				throw new FileNotFoundException("cannot find resource '" + uri +
						"' in classpath");
			}
			try {
				saxParser.parse(is, this);
			} finally {
				is.close();
			}
		} catch (final ParserConfigurationException t) {
			LOGGER.error(t);
		} catch (final IOException e) {
			LOGGER.error(e);
			throw new SAXException(e);
		}

		return my_action_list;
	}

	@Override
	public void startDocument() {
		// no change
	}

	@Override
	public void endDocument() {
		// no change
	}

	@Override
	public void startElement(final String namespaceURI, final String lName, final String qName,
							 final Attributes attrs) {
		value_text = "";
		if (qName.equals("action")) {
			attributes = new ArrayList < SlashAttributes > ();
			input_paramater = null;
			my_value = "";
			type_literal = "";
			command = "";
			type_param = -1;
			tag_of_attr = false;
			min_param = -1;
			max_param = -1;
			type_of_action = "";
			use_of_remainder = false;

		} else if (qName.equals("command")) {
			String value = attrs.getValue("value");
			command = value;
		} else if (qName.equals("use_of_remainder")) {
			String value = attrs.getValue("value");
			if (value.equals("True")) {
				use_of_remainder = true;
			} else if (value.equals("False")) {
				use_of_remainder = false;
			}
		} else if (qName.equals("min_param")) {
			String value = attrs.getValue("value");
			min_param = Integer.parseInt(value);
		} else if (qName.equals("max_param")) {
			String value = attrs.getValue("value");
			max_param = Integer.parseInt(value);
		} else if (qName.equals("action_attribute")) {
			my_value = "";
			type_literal = "";
			type_param = -1;
			tag_of_attr = true;
			input_paramater = null;

		} else if (tag_of_attr) {
			if (qName.equals("attribute_name")) {
				String value = attrs.getValue("value");
				my_value = value;

			} else if (qName.equals("attribute_value")) {
				String type = attrs.getValue("type");
				if (type.equals("literal")) {
					input_paramater = ParamNames.LITERAL;
					String value = attrs.getValue("value");
					type_literal = value;
				} else if (type.equals("param")) {
					input_paramater = ParamNames.PARAM;
					String value = attrs.getValue("value");
					type_param = Integer.parseInt(value);
				} else if (type.equals("remainder")) {
					input_paramater = ParamNames.REMAINDER;
				}
			}
		}
	}

	@Override
	public void endElement(final String namespaceURI, final String sName, final String qName) {
		if (qName.equals("action")) {
			if (type_of_action != null && !type_of_action.isEmpty() &&
					command != null && !command.isEmpty()) {
				DefaultAction our_slash_action = new DefaultAction(min_param, max_param, type_of_action);
				our_slash_action.set_is_reminder(use_of_remainder);
				our_slash_action.set_our_attr(attributes);
				our_slash_action.setCommand(command);
				my_action_list.add(our_slash_action);
			}

		} else if (tag_of_attr) {
			if (qName.equals("action_attribute")) {
				switch (input_paramater) {
					case LITERAL:
						if (my_value != null && type_literal != null)
							if (my_value.equals("type"))
								type_of_action = type_literal;
							else
								attributes.add(SlashAttributes.createSlashAttributes(my_value, type_literal));
						break;
					case PARAM:
						if (my_value != null && type_param != -1)
							attributes.add(SlashAttributes.createSlashAttributes(my_value, type_param));
						break;
					case REMAINDER:
						if (my_value != null)
							attributes.add(SlashAttributes.createSlashAttributes(my_value));
						break;
				}
			}

		}
	}

	@Override
	public void characters(final char[] buf, final int offset, final int len) {
		value_text = value_text + (new String(buf, offset, len)).trim();
	}

}