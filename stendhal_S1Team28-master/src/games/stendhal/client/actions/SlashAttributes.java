package games.stendhal.client.actions;

public class SlashAttributes {
	private ParamNames input_paramater;
	private String value;
	private String type_literal;
	private int type_param;

	/**
	 * Slash Attribute that can be used in DefaultAction to store required information about the paramaters.
	 */
	public SlashAttributes(ParamNames input_paramater, String value, int type_param) {
		this.input_paramater = input_paramater;
		this.value = value;
		this.type_param = type_param;
	}

	public SlashAttributes(ParamNames input_paramater, String value, String type_literal) {
		this.input_paramater = input_paramater;
		this.value = value;
		this.type_literal = type_literal;
	}

	public SlashAttributes(ParamNames input_paramater, String value) {
		this.input_paramater = input_paramater;
		this.value = value;
	}

	public String getTypeLiteral() {
		return input_paramater == ParamNames.LITERAL ? type_literal : null;
	}

	public Integer getTypeParam() {
		return input_paramater == ParamNames.PARAM ? type_param : null;
	}


	public static SlashAttributes createSlashAttributes(String value) {
		return new SlashAttributes(ParamNames.REMAINDER, value);
	}


	public static SlashAttributes createSlashAttributes(String value, String type_literal) {
		return new SlashAttributes(ParamNames.LITERAL, value, type_literal);
	}

	public static SlashAttributes createSlashAttributes(String value, int type_param) {
		return new SlashAttributes(ParamNames.PARAM, value, type_param);
	}

	public ParamNames getInput_paramater() {
		return input_paramater;
	}

	public String getValue() {
		return value;
	}



}


